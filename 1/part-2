#! /usr/bin/env python3

""" AoC 2021 - Day 1 Part 1 """

################################################################################

import argparse
import logging

################################################################################

def main():
    """ Main function """

    parser = argparse.ArgumentParser(description='AoC 2021')
    parser.add_argument('--debug', '-d', action='store_true', help='Enable debug')
    parser.add_argument('infile', help='Input file')

    args = parser.parse_args()

    logging.basicConfig(level=logging.DEBUG if args.debug else logging.INFO)

    readings = []

    with open(args.infile, 'rt') as infile:
        for data in infile.readlines():
            readings.append(int(data))

    previous = 0
    first = True
    increase = 0

    for i in range(len(readings)-2):
        depth = readings[i] + readings[i+1] + readings[i+2]

        if not first and depth > previous:
            increase += 1

        logging.debug('Depth: %d' % depth)

        first = False
        previous = depth

    print(f'Number of increasing values: {increase}')

################################################################################

if __name__ == '__main__':
    try:
        main()

    except KeyboardInterrupt:
        sys.exit(1)
    except BrokenPipeError:
        sys.exit(2)
