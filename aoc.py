#! /usr/bin/env python3

""" AoC 2021 Utility Module """

################################################################################

import argparse
import logging
import sys

################################################################################

def configure():
    """ Main function """

    parser = argparse.ArgumentParser(description='AoC 2021')
    parser.add_argument('--debug', '-d', action='store_true', help='Enable debug')
    parser.add_argument('infile', help='Input file')

    args = parser.parse_args()

    logging.basicConfig(level=logging.DEBUG if args.debug else logging.INFO)

    return args

################################################################################

def error(msg, status=1):
    """ Report an error and exit """

    print(f'ERROR: {msg}')
    sys.exit(status)
